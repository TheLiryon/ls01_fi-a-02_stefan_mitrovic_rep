/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Stefan Mitrovic >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 300000000000;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    short alterTage = 8395;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17100000; //approx size of Russia
    
    // Wie groß ist das kleinste Land der Erde?
    float flaecheKleinsteLand = 0.49; //in km² (49 ha) Vatican City
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);

    System.out.println("Einwohner in Berlin: " + bewohnerBerlin);

    System.out.println("Mein alter in Tagen: " + alterTage);

    System.out.println("(Max.) Gewicht des schwersten tieres (Blauwal): " + gewichtKilogramm + "kg");

    System.out.println("Fläche des größten Landes: " + flaecheGroessteLand + "km²");

    System.out.println("Fläche des kleinsten Landes: " + flaecheKleinsteLand + "km²");

    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

