public class Hund {
    private String name;
    private String rasse;
    private double größe;

    public Hund(String name, String rasse, double größe){
        this.name = name;
        this.rasse = rasse;
        this.größe = größe;
    }

    public void setName(String neuerName){
        this.name = neuerName;
    }

    public void belle(){
        System.out.println("Wau Wau!");
    }
}
