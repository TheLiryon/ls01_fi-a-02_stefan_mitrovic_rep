import java.util.Calendar;

public class BenutzerTest {
    public static void main(String[] args) {
        Benutzer benutzerEins = new Benutzer("eins");
        Benutzer benutzerZwei = new Benutzer("zwei");
        Benutzer benutzerDrei = new Benutzer("drei");
        Benutzer benutzerVier = new Benutzer("vier");
        Benutzer benutzerFuenf = new Benutzer("fünf");
        Benutzer benutzerSechs = new Benutzer("sechs");

        System.out.println(benutzerEins.getName());
        benutzerEins.setName("Eins Neu");
        System.out.println(benutzerEins.getName());

        benutzerZwei.setPasswort("Passwort");
        System.out.println(benutzerZwei.getPasswort());

        benutzerDrei.setEmailAddresse("something@something.de");
        System.out.println(benutzerDrei.getEmailAddresse());

        benutzerVier.setBerechtigungsStatus(true);
        System.out.println(benutzerVier.getBerechtigungsStatus().toString());

        benutzerFuenf.setAnmeldeStatus(true);
        System.out.println(benutzerFuenf.getAnmeldeStatus().toString());

        benutzerSechs.setLetzteAnmeldung(Calendar.getInstance().getTime());
        System.out.println(benutzerSechs.getLetzteAnmeldung().toString());
    }
}
