import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        Benutzer neuerBenutzer = new Benutzer("");

        neuerBenutzer.setName(scanner.nextLine());
        neuerBenutzer.setPasswort(scanner.nextLine());

        System.out.println("Neuer Benutzer name: " + neuerBenutzer.getName() + "\nNeuer Benutzer passwort: " + neuerBenutzer.getPasswort());
    }
}
