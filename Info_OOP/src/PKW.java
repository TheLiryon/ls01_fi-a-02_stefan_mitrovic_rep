public abstract class PKW {
    private String modell;
    private String farbe;
    private String hupSoundPath;

    public PKW(String modell){
        this.modell = modell;
    }

    public String getFarbe() {
        return farbe;
    }
    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }

    public String getHupSoundPath() {
        return hupSoundPath;
    }
    public void setHupSoundPath(String hupSoundPath) {
        this.hupSoundPath = hupSoundPath;
    }

    public void fahre(double geschwindigkeit){
        System.out.println(modell + " fährt mit geschwindigkeit " + geschwindigkeit + "km/h");
    }
    public void lenke(double richtung){
        richtung = richtung % 360;
        System.out.println(modell + " lenkt in richtung " + richtung + "°");
    }
    public void hupe(double dauer){
        System.out.println(modell + " hupt mit file " + hupSoundPath + " für " + dauer + " Sekunde(n)");
    }
}
