public class PKWTest {
    public static void main(String[] args){
        BugattiVeyron veyron = new BugattiVeyron("veyron");
        veyron.setFarbe("rot");
        veyron.setHupSoundPath("./crazyfrog.mp3");
        TeslaS tesla = new TeslaS("S");
        tesla.setFarbe("schwarz");
        tesla.setHupSoundPath("./bruh.mp3");
        Ente ente = new Ente("ente");

        ente.setFarbe("grün");
        System.out.println(ente.getFarbe());

        veyron.lenke(200);
        veyron.fahre(180);
        ente.lenke(380);
        ente.fahre(200);

        tesla.hupe(5);
        veyron.hupe(10);
        ente.setHupSoundPath("./quack.mp3");
        ente.hupe(1);

        System.out.println(ente.getHupSoundPath());
    }
}
