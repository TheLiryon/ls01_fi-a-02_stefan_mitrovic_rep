import java.util.Date;

public class Benutzer {
    private String name;
    private String passwort;
    private String emailAddresse;
    private Boolean berechtigungsStatus;
    private Boolean anmeldeStatus;
    private Date letzteAnmeldung;

    public Benutzer(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPasswort() {
        return passwort;
    }
    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getEmailAddresse() {
        return emailAddresse;
    }
    public void setEmailAddresse(String emailAddresse) {
        this.emailAddresse = emailAddresse;
    }

    public Boolean getBerechtigungsStatus() {
        return berechtigungsStatus;
    }
    public void setBerechtigungsStatus(Boolean berechtigungsStatus) {
        this.berechtigungsStatus = berechtigungsStatus;
    }

    public Boolean getAnmeldeStatus() {
        return anmeldeStatus;
    }
    public void setAnmeldeStatus(Boolean anmeldeStatus) {
        this.anmeldeStatus = anmeldeStatus;
    }

    public Date getLetzteAnmeldung() {
        return letzteAnmeldung;
    }
    public void setLetzteAnmeldung(Date letzteAnmeldung) {
        this.letzteAnmeldung = letzteAnmeldung;
    }
}
