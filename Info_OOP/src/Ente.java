public class Ente extends PKW {
    public Ente(String modell) {
        super(modell);
    }

    @Override
    public void fahre(double geschwindigkeit){
        if(geschwindigkeit >= 180) geschwindigkeit = 179;
        super.fahre(geschwindigkeit);
    }
}
