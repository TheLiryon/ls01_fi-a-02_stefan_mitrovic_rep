import java.util.Scanner;

class Fahrkartenautomat {

    public static void main(String[] args) {
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

        fahrkartenAusgeben();

        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir w�nschen Ihnen eine gute Fahrt.");
    }

    private static double fahrkartenbestellungErfassen(){
        Scanner tastatur = new Scanner(System.in);

        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        //Mengentest
        if(zuZahlenderBetrag < 0.05){
            System.out.println("FEHLER: Ein Ticket kann nicht weniger als 5 CENT kosten!");
            zuZahlenderBetrag = 0.05;
        }

        System.out.print("Azahl der Tickets: ");
        int ticketAnzahl = tastatur.nextInt();
        //Mengentest
        if (ticketAnzahl > 10) {
            System.out.println("FEHLER: Sie k�nnen nicht mehr as 10 tickets auf ein mal kaufen!");
            ticketAnzahl = 1;
        } else if (ticketAnzahl < 1) {
            System.out.println("FEHLER: Sie k�nnen nicht weniger als 1 ticket kaufen!");
            ticketAnzahl = 1;
        }

        zuZahlenderBetrag *= ticketAnzahl;
        return zuZahlenderBetrag;
    }

    private static double fahrkartenBezahlen(double zuZahlen){
        Scanner tastatur = new Scanner(System.in);

        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        return eingezahlterGesamtbetrag;
    }

    private static void fahrkartenAusgeben(){
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    private static void rueckgeldAusgeben(double rueckgabebetrag){
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if (rueckgabebetrag > 0.0) {
            System.out.println("Der R�ckgabebetrag in H�he von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }
}